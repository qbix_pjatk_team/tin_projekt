import React, {Component} from 'react';
import ReactTable from 'react-table';
import * as mediaRepository from '../../../repositories/mediaRepository';
import * as mediaTypeRepository from '../../../repositories/mediaTypeRepository';
import 'react-table/react-table.css';
import ActionsCellRenderer from "./ActionsCellRenderer";
import CheckBoxCellRenderer from "./CheckBoxCellRenderer";

export default class CatalogPanel extends Component {
    state = {
        data: [],
        mediaTypes: {}
    };

    componentDidMount() {
        mediaRepository.getAllMedia()
            .then(data => this.setState({
                data: data
            }));
        mediaTypeRepository.getMediaTypes()
            .then(types => this.setState({
                mediaTypes: types
            }));
    }

    render() {
        return (
            <div className="panel">
                <ReactTable
                    filterable={true}
                    data={this.state.data}
                    columns={this.columnDefinition(this.props.user)}
                    defaultPageSize={5}
                    nextText="Następna strona"
                    previousText="Poprzednia strona"
                    rowsText="wierszy"
                    pageText="Strona"
                    ofText="z"
                />
            </div>
        );
    }

    caseInsensitiveFilter = (filter, row) => {
        return row[filter.id].toLowerCase().includes(filter.value.toLowerCase());
    };

    columnDefinition = (user) => {
        let userRole = user ? user.userRole : "";
        return [
            {
                Header: "Zaznacz",
                accessor: "checkbox",
                filterable: false,
                sortable: false,
                Cell: CheckBoxCellRenderer(this.props.selectedIds, this.props.changeSelection),
                width: 60
            },
            {
                Header: "Rodzaj",
                accessor: "mediaType",
                Cell: (row) => {
                    let val = Object.values(this.state.mediaTypes)
                        .filter(type => type.name === row.row.mediaType)[0];
                    if (val !== undefined) {
                        return <div>{val.label}</div>;
                    } else {

                        return <div></div>;
                    }
                },
                filterMethod: this.caseInsensitiveFilter
            },
            {
                Header: "Tytuł",
                accessor: "title",
                filterMethod: this.caseInsensitiveFilter
            },
            {
                Header: "Autor",
                accessor: "author",
                filterMethod: this.caseInsensitiveFilter
            },
            {
                Header: "Status",
                accessor: "status",
                filterMethod: this.caseInsensitiveFilter
            },
            {
                Header: "Akcje",
                accessor: "actions",
                filterable: false,
                sortable: false,
                Cell: ActionsCellRenderer(this.props.changeComponentTrigger, userRole)
            }
        ];
    };
}
