import httpClient from "../utils/HttpClient";
import {baseUrl} from "../utils/settings";

const userUrl = `${baseUrl}/borrow`;

export const borrow = (borrows) => {
    return httpClient.post(userUrl, borrows);
};

export const returnBorrows = (mediumIds) => {
    return httpClient.put(userUrl, mediumIds);
};
