create table FIELD(
                      ID               IDENTITY    NOT NULL PRIMARY KEY,
                      MEDIA_TYPE       VARCHAR(10) NOT NULL,
                      NAME             VARCHAR(20) NOT NULL,
                      LABEL            VARCHAR(20) NOT NULL,
                      REQUIRED         BOOLEAN     NOT NULL,
                      DATA_TYPE        VARCHAR(10) NOT NULL,
                      AVAILABLE_VALUES VARCHAR(255)
)
