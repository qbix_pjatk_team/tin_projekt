import React, {Component} from 'react';

export default class SingleValueField extends Component {
    render() {
        const {field, data} = this.props;
        return (
            <input type="text"
                   name={field.name}
                   value={data}
                   onChange={this.props.onChange}
                   onBlur={this.props.onBlur}/>
        );
    }
}
