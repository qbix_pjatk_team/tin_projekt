package pl.qbix.pjatk.tin

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import pl.qbix.pjatk.tin.repositories.FieldRepository

import static org.hamcrest.Matchers.hasSize
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class FieldControllerTest extends IntegrationTest {
    @Autowired
    private FieldRepository fieldRepository
    
    def "should contain all fields from contract"() {
        when:
            def resultActions = mockMvc.perform(get('/field'))
        
        then:
            resultActions
                    .andExpect(status().isOk())
                    .andExpect(jsonPath('$[0].id').isNumber())
                    .andExpect(jsonPath('$[0].name').isString())
                    .andExpect(jsonPath('$[0].label').isString())
                    .andExpect(jsonPath('$[0].mediaType').isString())
                    .andExpect(jsonPath('$[0].dataType').isString())
                    .andExpect(jsonPath('$[0].required').isBoolean())
                    .andExpect(jsonPath('$[0].availableValues').isArray())
    }
    
    def "should return all fields by media type"() {
        when:
            def resultAction = mockMvc.perform(get("/field?mediaType=book"))
        
        then:
            resultAction
                    .andExpect(status().isOk())
                    .andExpect(jsonPath('$').isArray())
                    .andExpect(jsonPath('$', hasSize(2)))
    }
    
    def "should allow to create new field"() {
        given:
            def fieldValue = [
                    mediaType      : "movie",
                    name           : "someField",
                    label          : "someLabel",
                    required       : true,
                    dataType       : "STRING",
                    availableValues: ["value1", "value2"]]
        
        when:
            def result = mockMvc.perform(post("/field")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(fieldValue)))
        
        then:
            result.andExpect(status().isCreated())
            def id = getId(result)
            fieldRepository.findById(id).isDefined()
    }
    
    def "should allow to update field"() {
        given:
            def fieldValue = [
                    mediaType      : "movie",
                    name           : "someField",
                    label          : "someLabel",
                    required       : true,
                    dataType       : "STRING",
                    availableValues: ["value1", "value2"]]
            def result = mockMvc.perform(post("/field")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(fieldValue)))
            def id = getId(result)
        when:
            fieldValue.name = 'some other name'
            fieldValue.availableValues = []
            result = mockMvc.perform(put("/field/${id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(fieldValue)))
        
        then:
            result.andExpect(status().isNoContent())
            def field = fieldRepository.findById(id).get()
            field.getName() == 'some other name'
            field.availableValues.isEmpty()
    }
    
    def "should return not found exception when trying to update non existing field"() {
        given:
            def fieldValue = [
                    mediaType      : "movie",
                    name           : "someField",
                    label          : "someLabel",
                    required       : true,
                    dataType       : "STRING",
                    availableValues: ["value1", "value2"]]
            def result = mockMvc.perform(post("/field")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(fieldValue)))
            def id = getId(result) + 1
        when:
            result = mockMvc.perform(put("/field/${id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(fieldValue)))
        
        then:
            result.andExpect(status().isNotFound())
    }
    
    def "should allow to delete field"() {
        given:
            def fieldValue = [
                    mediaType      : "movie",
                    name           : "someField",
                    label          : "someLabel",
                    required       : true,
                    dataType       : "STRING",
                    availableValues: ["value1", "value2"]]
            def result = mockMvc.perform(post("/field")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(fieldValue)))
            def id = getId(result)
        
        when:
            result = mockMvc.perform(delete("/field/${id}"))
        
        then:
            result.andExpect(status().isNoContent())
            fieldRepository.findById(id).isEmpty()
    }
}
