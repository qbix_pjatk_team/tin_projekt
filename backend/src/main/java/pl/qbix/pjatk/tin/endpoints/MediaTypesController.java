package pl.qbix.pjatk.tin.endpoints;

import io.vavr.Tuple2;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.qbix.pjatk.tin.dto.MediaTypeDTO;
import pl.qbix.pjatk.tin.mappers.MediaTypeMapper;
import pl.qbix.pjatk.tin.model.MediaType;

import java.util.Arrays;

@RestController
@RequestMapping("/media-type")
@AllArgsConstructor
public class MediaTypesController {
    private final MediaTypeMapper mapper;

    @GetMapping
    public Map<String, MediaTypeDTO> getMediaTypes() {
        return List.ofAll(Arrays.stream(MediaType.values()))
                .map(mapper::toDto)
                .toMap((dto) -> new Tuple2<>(dto.getName(), dto));
    }
}
