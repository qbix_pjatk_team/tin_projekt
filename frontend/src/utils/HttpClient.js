class HttpClient {
    get = (url) => {
        return fetch(url)
            .then(this.handleErrors)
            .then(data => {
                return data.json();
            });
    };

    post = (url, body, expectResponse = false) => {
        return fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(this.handleErrors)
            .then(data => {
                if (expectResponse) {
                    return data.json();
                } else {
                    return data;
                }
            });
    };

    put = (url, body) => {
        return fetch(url, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        })
            .then(this.handleErrors);
    };

    delete = (url) => {
        return fetch(url, {
            method: 'DELETE'
        })
            .then(this.handleErrors);
    };

    handleErrors = async (response) => {
        if (!response.ok) {
            throw Error((await response.json()).message);
        }
        return response;
    };
}

const httpClient = new HttpClient();

export default httpClient;
