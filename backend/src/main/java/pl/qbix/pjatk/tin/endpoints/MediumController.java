package pl.qbix.pjatk.tin.endpoints;

import io.vavr.collection.Map;
import io.vavr.collection.Traversable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.qbix.pjatk.tin.mappers.MediumMapper;
import pl.qbix.pjatk.tin.repositories.MediumRepository;

import javax.transaction.Transactional;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/medium")
@AllArgsConstructor
@Slf4j
public class MediumController {
    MediumRepository mediumRepository;
    MediumMapper mediumMapper;

    @GetMapping
    public Traversable<Map<String, Object>> getMedia() {
        return mediumRepository.findAll()
                .map(medium -> mediumMapper.toDto(medium));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Map<String, Object>> getMedia(@PathVariable Long id) {
        return mediumRepository.findById(id)
                .map(medium -> mediumMapper.toDto(medium))
                .map(ResponseEntity::ok)
                .getOrElse(ResponseEntity.notFound().build());
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Long createMedia(@RequestBody Map<String, Object> medium) {
        return mediumRepository.save(mediumMapper.toMedium(medium)).getId();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteMedia(@PathVariable Long id) {
        mediumRepository.deleteById(id);
    }

    @DeleteMapping
    @ResponseStatus(NO_CONTENT)
    @Transactional
    public void deleteMedia(@RequestParam java.util.Set<Long> ids) {
        mediumRepository.deleteByIdIn(ids);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateMedia(@PathVariable Long id, @RequestBody Map<String, Object> mediumMap) {
        return mediumRepository.findById(id)
                .peek(medium -> {
                    var newMedium = mediumMapper.toMedium(mediumMap);
                    medium.setAuthor(newMedium.getAuthor());
                    medium.setMediaType(newMedium.getMediaType());
                    medium.setStatus(newMedium.getStatus());
                    medium.setTitle(newMedium.getTitle());
                    medium.setPublishDate(newMedium.getPublishDate());
                    medium.setAgeLimitation(newMedium.getAgeLimitation());
                    medium.setBarcode(newMedium.getBarcode());
                    medium.setTypeSpecificFields(newMedium.getTypeSpecificFields().stream().peek(field -> field.setMedium(medium)).collect(Collectors.toList()));
                    mediumRepository.save(medium);
                })
                .map(medium -> ResponseEntity.noContent().build())
                .getOrElse(() -> ResponseEntity.notFound().build());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handle(IllegalArgumentException ex) {
        log.error("Problems during execution", ex);
        return ResponseEntity.badRequest()
                .body(ex.getMessage());
    }
}
