package pl.qbix.pjatk.tin.mappers;

import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.qbix.pjatk.tin.dto.MediaTypeDTO;
import pl.qbix.pjatk.tin.model.MediaType;

@Service
public class MediaTypeMapper {
    public MediaTypeDTO toDto(MediaType mediaType) {
        var mapping = Mappings.valueOf(mediaType.name());
        return MediaTypeDTO.builder()
                .name(mapping.tag)
                .label(mapping.label)
                .build();
    }

    public MediaType fromString(String mediaType) {
        return List.of(Mappings.values())
                .find(type -> type.tag.equals(mediaType))
                .map(type -> MediaType.valueOf(type.name()))
                .getOrElseThrow(() -> new IllegalArgumentException("Missing media type " + mediaType));
    }

    @AllArgsConstructor
    private enum Mappings {
        BOOK("book", "Książka"),
        MOVIE("movie", "Film");

        private String tag;
        private String label;
    }
}
