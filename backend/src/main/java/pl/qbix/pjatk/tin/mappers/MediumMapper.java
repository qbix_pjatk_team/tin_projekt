package pl.qbix.pjatk.tin.mappers;

import io.vavr.collection.HashMap;
import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.control.Option;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.qbix.pjatk.tin.model.Field;
import pl.qbix.pjatk.tin.model.FieldValue;
import pl.qbix.pjatk.tin.model.Medium;
import pl.qbix.pjatk.tin.repositories.FieldRepository;

import java.time.LocalDate;

@Service
@AllArgsConstructor
public class MediumMapper {
    private final MediaTypeMapper mediaTypeMapper;
    private final StatusMapper statusMapper;
    private final FieldRepository fieldRepository;

    public Map<String, Object> toDto(Medium medium) {
        HashMap<String, Object> dto = HashMap.of(
                "id", medium.getId(),
                "mediaType", mediaTypeMapper.toDto(medium.getMediaType()).getName(),
                "title", medium.getTitle(),
                "author", medium.getAuthor(),
                "status", statusMapper.toDto(medium.getStatus()),
                "publishDate", medium.getPublishDate(),
                "ageLimitation", Option.of(medium.getAgeLimitation()).map(Object::toString).getOrNull(),
                "barcode", medium.getBarcode()
        );
        var fieldValues = List.ofAll(medium.getTypeSpecificFields())
                .toMap(value -> value.getField().getName(), FieldValue::getValue);
        return dto.merge(fieldValues);
    }

    public Medium toMedium(Map<String, Object> mediumObject) {
        var mediaType = mediaTypeMapper.fromString(getAsString(mediumObject, "mediaType"));
        var medium = Medium.builder()
                .mediaType(mediaType)
                .title(getAsString(mediumObject, "title"))
                .author(getAsString(mediumObject, "author"))
                .status(statusMapper.fromString(getAsString(mediumObject, "status")))
                .publishDate(LocalDate.parse(getAsString(mediumObject, "publishDate")))
                .ageLimitation(getAsInteger(mediumObject, "ageLimitation"))
                .barcode(getAsString(mediumObject, "barcode"))
                .build();
        var typeSpecificFields = fieldRepository.findAllByMediaType(mediaType)
                .map(field -> toFieldValue(field, mediumObject, medium))
                .toJavaList();
        medium.setTypeSpecificFields(typeSpecificFields);
        return medium;
    }

    private FieldValue toFieldValue(Field field, Map<String, Object> mediumObject, Medium medium) {
        var value = mediumObject.get(field.getName()).map(Object::toString).getOrNull();
        if (value == null && field.isRequired()) {
            throw new IllegalArgumentException("Missing value for field " + field.getName());
        }
        if (!field.getAvailableValues().equals("") && value != null) {
            var availableValues = List.of(field.getAvailableValues().split("; "));
            if (!availableValues.contains(value)) {
                throw new IllegalArgumentException(String.format("Only available values for field %s are %s but found %s", field.getName(), availableValues, value));
            }
        }
        return FieldValue.builder()
                .field(field)
                .medium(medium)
                .value(mediumObject.get(field.getName()).map(Object::toString).getOrNull())
                .build();
    }

    private String getAsString(Map<String, Object> medium, String key) {
        return medium.get(key).map(Object::toString).getOrNull();
    }

    private Integer getAsInteger(Map<String, Object> medium, String key) {
        var value = getAsString(medium, key);
        if (value == null) {
            return null;
        } else {
            return Integer.parseInt(value);
        }
    }
}
