import React, {Component} from 'react';
import UserSummary from "./UserSummary";
import MenuItem from "../Sidebar/MenuItem";
import {componentNames} from "../MainApp";

export default class UserInfoPanel extends Component {
    render() {
        return (
            <div className={"user-info-panel"}>
                {this.isLoggedIn() &&
                <UserSummary user={this.props.user} fireComponentChange={this.props.fireComponentChange}/>}
                {this.isLoggedIn() &&
                <MenuItem
                    label="Wyloguj"
                    clickHandler={this.logout}/>}
                {!this.isLoggedIn() && <MenuItem
                    label="Zaloguj"
                    clickHandler={() => this.props.fireComponentChange(componentNames.LOGIN_PAGE)}
                />}
                {!this.isLoggedIn() && <MenuItem
                    label="Zarejestruj"
                    clickHandler={() => this.props.fireComponentChange(componentNames.REGISTER_PAGE)}
                />}
            </div>
        );
    }

    isLoggedIn = () => {
        return this.props.user != null;
    };

    logout = () => {
        const {logoutHandler} = this.props;
        logoutHandler();
    };
}
