package pl.qbix.pjatk.tin

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer
import io.vavr.jackson.datatype.VavrModule
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.ResultActions
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext
import spock.lang.Specification

import javax.transaction.Transactional
import java.nio.charset.Charset
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@SpringBootTest(classes = [Mediateka.class, TestConfig.class])
@Transactional
class IntegrationTest extends Specification {
    @Autowired
    protected MockMvc mockMvc
    
    protected static String toJson(Object o) {
        def mapper = new ObjectMapper()
        mapper.registerModule(new VavrModule())
        def javaTimeModule = new JavaTimeModule()
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ISO_DATE))
        mapper.registerModule(javaTimeModule)
        return mapper.writeValueAsString(o)
    }
    
    protected static long getId(ResultActions result) {
        Long.parseLong(
                result.andReturn()
                        .getResponse()
                        .getContentAsString(Charset.forName("UTF-8")))
    }
    
    @TestConfiguration
    static class TestConfig {
        @Bean
        MockMvc mockMvc(WebApplicationContext context) {
            return MockMvcBuilders.webAppContextSetup(context)
                    .alwaysDo(MockMvcResultHandlers.print())
                    .build()
        }
        
    }
    
}
