package pl.qbix.pjatk.tin.mappers;

import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.qbix.pjatk.tin.dto.FieldDTO;
import pl.qbix.pjatk.tin.model.Field;

import static java.util.stream.Collectors.joining;

@Service
@AllArgsConstructor
public class FieldMapper {
    private final MediaTypeMapper mediaTypeMapper;

    public FieldDTO toDto(Field field) {
        return
                FieldDTO.builder()
                        .id(field.getId())
                        .mediaType(mediaTypeMapper.toDto(field.getMediaType()).getName())
                        .name(field.getName())
                        .label(field.getLabel())
                        .required(field.isRequired())
                        .dataType(field.getDataType())
                        .availableValues(
                                toAvailableValuesList(field.getAvailableValues()))
                        .build();
    }

    private List<String> toAvailableValuesList(String availableValues) {
        if (availableValues.equals("")) {
            return List.empty();
        } else {
            return List.of(availableValues.split("; "));
        }
    }

    public Field fromDto(FieldDTO fieldDTO) {
        return Field.builder()
                .mediaType(mediaTypeMapper.fromString(fieldDTO.getMediaType()))
                .name(fieldDTO.getName())
                .label(fieldDTO.getLabel())
                .required(fieldDTO.isRequired())
                .dataType(fieldDTO.getDataType())
                .availableValues(fieldDTO.getAvailableValues().collect(joining("; ")))
                .build();
    }
}
