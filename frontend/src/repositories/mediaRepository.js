import httpClient from "../utils/HttpClient";
import {baseUrl} from "../utils/settings";

const mediaUrl = `${baseUrl}/medium`;

export const getAllMedia = () => {
    return httpClient.get(mediaUrl);
};

export const save = (medium) => {
    if (medium.id === null || medium.id === undefined) {
        return httpClient.post(mediaUrl, medium);
    } else {
        return httpClient.put(`${mediaUrl}/${medium.id}`, medium);
    }
};

export const deleteIds = (ids) => {
    return httpClient.delete(`${mediaUrl}?ids=${ids}`);
};


