package pl.qbix.pjatk.tin.endpoints;

import io.vavr.collection.List;
import io.vavr.collection.Traversable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.qbix.pjatk.tin.dto.FieldDTO;
import pl.qbix.pjatk.tin.mappers.FieldMapper;
import pl.qbix.pjatk.tin.mappers.MediaTypeMapper;
import pl.qbix.pjatk.tin.repositories.FieldRepository;
import pl.qbix.pjatk.tin.repositories.MediumRepository;

import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("/field")
@AllArgsConstructor
@Slf4j
public class FieldController {
    private final FieldRepository fieldRepository;
    private final MediumRepository mediumRepository;
    private final FieldMapper fieldMapper;
    private final MediaTypeMapper mediaTypeMapper;

    @GetMapping
    public Traversable<FieldDTO> getAllFields() {
        return fieldRepository.findAll()
                .map(fieldMapper::toDto);
    }

    @GetMapping(params = {"mediaType"})
    public Traversable<FieldDTO> getByMediaType(@RequestParam String mediaType) {
        return fieldRepository
                .findAllByMediaType(mediaTypeMapper.fromString(mediaType))
                .map(fieldMapper::toDto);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Long createField(@RequestBody FieldDTO fieldDTO) {
        return fieldRepository
                .save(fieldMapper.fromDto(fieldDTO))
                .getId();
    }

    @PutMapping
    @ResponseStatus(NO_CONTENT)
    public void updateFields(@RequestBody List<FieldDTO> fields) {
        var newFields = fields.filter(field -> field.getId() == null);
        newFields.forEach(this::createField);
        var existingFields = fields.filter(field -> field.getId() != null);
        existingFields.forEach(field -> updateField(field.getId(), field));
    }

    @PutMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public ResponseEntity updateField(@PathVariable Long id, @RequestBody FieldDTO fieldDto) {
        return fieldRepository.findById(id)
                .peek(field -> {
                    var newField = fieldMapper.fromDto(fieldDto);
                    field.setName(newField.getName());
                    field.setLabel(newField.getLabel());
                    field.setDataType(newField.getDataType());
                    field.setMediaType(newField.getMediaType());
                    field.setRequired(newField.isRequired());
                    field.setAvailableValues(newField.getAvailableValues());
                    fieldRepository.save(field);
                })
                .map(field -> ResponseEntity.noContent().build())
                .getOrElse(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteField(@PathVariable Long id) {
        mediumRepository.findAll()
                .peek(medium -> {
                    medium.setTypeSpecificFields(medium.getTypeSpecificFields()
                            .stream()
                            .filter(field -> !field.getField().getId().equals(id))
                            .collect(Collectors.toUnmodifiableList()));
                })
                .forEach(mediumRepository::save);
        fieldRepository.deleteById(id);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(BAD_REQUEST)
    public String badRequest(IllegalArgumentException ex) {
        return ex.getMessage();
    }

}
