package pl.qbix.pjatk.tin.dto;

import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.qbix.pjatk.tin.model.DataType;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FieldDTO {
    Long id;
    String mediaType;
    String name;
    String label;
    boolean required;
    DataType dataType;
    List<String> availableValues;
}
