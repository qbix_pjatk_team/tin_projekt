import React, {Component} from 'react';
import FieldItem from "./FieldItem";
import * as fieldsRepository from '../../repositories/fieldsRepository';
import {componentNames} from "../../MainApp";

export default class MediaTypePanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: [],
            lastFieldId: -1
        };
        fieldsRepository.getFieldsByMediaType(props.mediaType.name)
            .then(data => this.setState({fields: [...data]}));
    }

    render() {
        const {mediaType} = this.props;
        return (
            <div className="media-type-panel">
                <span className="header">
                    {mediaType.label}
                    <button onClick={this.saveChanges}>Zapisz</button>
                    <button onClick={this.addField}>Dodaj</button>
                </span>
                <span className="field-items">
                    <div className="field-item-header">
                        <span>nazwa</span>
                        <span>etykieta</span>
                        <span>wymagane</span>
                        <span>typ danych</span>
                        <span>dostępne wartości</span>
                    </div>
                    {this.getTypeFields()}
                </span>
            </div>
        );
    }

    getTypeFields = () => {
        return this.state.fields
            .sort((d1, d2) => d1.id - d2.id)
            .map(
                field => <FieldItem field={field} key={field.id}
                                    changeComponentTrigger={this.props.changeComponentTrigger}
                                    changeHandler={this.getChangeHandler(field)}/>
            );
    };

    getChangeHandler = (originalField) => (event) => {
        const {name, value, type, checked} = event.target;
        let newValue = type === "checkbox" ? checked : value;
        if (name === "availableValues") {
            newValue = newValue.split(",");
        }
        let fields = [...this.state.fields];
        let field = {
            ...fields
                .filter(f => f.id === originalField.id)[0]
        };
        field[name] = newValue;
        fields = fields.filter(
            f => f.id !== originalField.id
        );
        console.log(["field", field]);
        fields.push(field);
        this.setState({fields});
    };

    saveChanges = () => {
        fieldsRepository.updateFields(this.state.fields)
            .then(() => this.props.changeComponentTrigger(componentNames.ADMINISTRATION_PAGE));
    };

    addField = () => {
        this.setState(prevState => {
            return {
                fields: [...prevState.fields,
                    {
                        id: this.state.lastFieldId,
                        mediaType: this.props.mediaType.name,
                        dataType: "STRING",
                        required: false,
                        availableValues: []
                    }],
                lastFieldId: this.state.lastFieldId - 1
            };
        });
    };
}
