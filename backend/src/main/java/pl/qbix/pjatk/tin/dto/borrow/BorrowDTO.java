package pl.qbix.pjatk.tin.dto.borrow;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BorrowDTO {
    Long userId;
    Long mediumId;
}
