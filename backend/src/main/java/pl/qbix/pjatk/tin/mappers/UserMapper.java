package pl.qbix.pjatk.tin.mappers;

import org.springframework.stereotype.Service;
import pl.qbix.pjatk.tin.dto.user.SimplifiedUserDTO;
import pl.qbix.pjatk.tin.dto.user.UserDTO;
import pl.qbix.pjatk.tin.model.user.User;

@Service
public class UserMapper {
    public User mapFromDto(UserDTO userDTO) {
        return User.builder()
                .name(userDTO.getLogin())
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .birthDate(userDTO.getBirthDate())
                .address(userDTO.getAddress())
                .gender(userDTO.getGender())
                .build();
    }

    public SimplifiedUserDTO toSimplifiedDTO(User user) {
        return SimplifiedUserDTO.builder()
                .id(user.getId())
                .username(user.getName())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .userRole(user.getRole())
                .build();
    }
}
