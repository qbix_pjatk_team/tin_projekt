#Settings
### Addresses
* http://localhost:3000 - frontend
* http://localhost:8080 - backend

### Data definition
The data are loaded into in memory database on each application run. The scripts creating schema and filling out the
 data are placed in `backend/src/main/resources/db/migration` folder

#Run frontend
### Node.js and npm installed:
Just run npm start from frontend directory

### Node.js and npm not installed:
Run from project root: `gradlew frontend:start`

Be aware that stopping gradle deamon will not stop the node.js process, and you'll need to do that manually from the
 task manager.

#Run backend
### Prerequisistes
* Installed JDK11 as default java

### Running the backend
Run gradlew `backend:bootRun`

### Stopping the backend
Just stop the process of gradle
