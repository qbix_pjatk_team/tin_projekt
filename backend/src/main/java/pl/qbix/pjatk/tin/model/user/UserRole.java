package pl.qbix.pjatk.tin.model.user;

public enum UserRole {
    USER, LIBRARIAN, ADMIN
}
