package pl.qbix.pjatk.tin.dto.user;

import lombok.Builder;
import lombok.Value;
import pl.qbix.pjatk.tin.model.user.UserRole;

@Value
@Builder
public class SimplifiedUserDTO {
    Long id;
    String username;
    UserRole userRole;
    String firstName;
    String lastName;
}
