package pl.qbix.pjatk.tin.repositories;

import io.vavr.collection.Traversable;
import io.vavr.control.Option;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.Repository;

import java.util.Collection;

@NoRepositoryBean
public interface MyBaseJPARepository<TYPE, ID> extends Repository<TYPE, ID> {
    Traversable<TYPE> findAll();

    Option<TYPE> findById(ID id);

    TYPE save(TYPE object);

    void deleteById(ID id);

    void deleteByIdIn(Collection<ID> ids);
}
