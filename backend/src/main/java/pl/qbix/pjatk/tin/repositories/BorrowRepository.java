package pl.qbix.pjatk.tin.repositories;

import io.vavr.collection.List;
import org.springframework.data.jpa.repository.Query;
import pl.qbix.pjatk.tin.model.Borrow;

import java.util.Collection;

public interface BorrowRepository extends MyBaseJPARepository<Borrow, Long> {
    @Query("select borrow from Borrow as borrow where borrow.medium.id = ?1")
    List<Borrow> findByMediumId(Long mediumId);

    @Query("select borrow from Borrow as borrow where borrow.medium.id in ?1")
    List<Borrow> findAllByMediumIdIn(Collection<Long> mediumIds);
}
