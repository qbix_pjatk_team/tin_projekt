import React, {Component} from 'react';
import MenuItem from "./MenuItem";
import "./sidebar.css";
import {componentNames} from "../MainApp";

export default class Sidebar extends Component {

    render() {
        const {user, changeComponentHandler, activeComponent} = this.props;
        return (<div className="sidebar">
            <span className="item">
                <MenuItem
                    label="Katalog"
                    clickHandler={() => changeComponentHandler(componentNames.CATALOG_PAGE)}
                    isActive={activeComponent === componentNames.CATALOG_PAGE}
                />
                {user && user.userRole === "ADMIN" &&
                <MenuItem
                    label="Administracja"
                    clickHandler={() => changeComponentHandler(componentNames.ADMINISTRATION_PAGE)}
                    isActive={activeComponent === componentNames.ADMINISTRATION_PAGE}
                />}
            </span>
        </div>);
    }
}
