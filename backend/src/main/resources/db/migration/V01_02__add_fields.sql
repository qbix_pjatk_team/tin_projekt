insert into FIELD (MEDIA_TYPE, NAME, LABEL, REQUIRED, DATA_TYPE, AVAILABLE_VALUES)
VALUES ('BOOK', 'genre', 'Gatunek', 'N', 'STRING', 'Kryminał; Obyczajowa; Fantasy');
insert into FIELD (MEDIA_TYPE, NAME, LABEL, REQUIRED, DATA_TYPE, AVAILABLE_VALUES)
VALUES ('BOOK', 'pages', 'Liczba stron', 'N', 'INTEGER', '');
insert into FIELD (MEDIA_TYPE, NAME, LABEL, REQUIRED, DATA_TYPE, AVAILABLE_VALUES)
VALUES ('MOVIE', 'medium', 'Nośnik', 'N', 'STRING', 'DVD; Blue-Ray; VHS')
