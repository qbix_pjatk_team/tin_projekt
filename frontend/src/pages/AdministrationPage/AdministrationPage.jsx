import React, {Component} from 'react';
import * as mediaTypeRepository from "../../repositories/mediaTypeRepository";
import MediaTypePanel from "./MediaTypePanel";
import './style.css';

export default class AdministartionPage extends Component {
    state = {
        mediaTypes: {}
    };

    componentDidMount() {
        mediaTypeRepository.getMediaTypes()
            .then(types => this.setState({mediaTypes: types}));
    }

    render() {
        return (
            <div className="administration-page">
                {this.getMediaTypePanels()}
            </div>
        );
    }

    getMediaTypePanels() {
        return Object.values(this.state.mediaTypes)
            .map(type => <MediaTypePanel key={type.name} mediaType={type}
                                         changeComponentTrigger={this.props.changeComponentTrigger}/>);
    }
}
