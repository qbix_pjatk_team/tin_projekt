import React, {Component} from 'react';
import {componentNames} from "../../MainApp";
import * as fieldsRepository from "../../repositories/fieldsRepository";

export default class DetailsPanel extends Component {
    state = {
        fields: []
    };

    constructor(props) {
        fieldsRepository.getFieldsByMediaType(props.data.mediaType)
            .then(fields => this.setState({fields: fields}));
        super();
    }

    render() {
        const {data} = this.props;
        return (
            <div className="details-panel">
                <button className='back-button'
                        onClick={() => this.props.changeComponentTrigger(componentNames.CATALOG_PAGE)}>Wróć
                </button>
                <div className="data">
                    {this.getPropertyItems(data)}
                </div>
            </div>
        );
    }

    getPropertyItems = (data) => {
        const entries = Object.entries(data);
        let values = [];
        for (let [key, value] of entries) {
            values.push(<label>{this.getFieldLabel(key)}:</label>);
            values.push(<label>{value}</label>);
        }
        return values;
    };

    getFieldLabel = (name) => {
        switch (name) {
            case 'title':
                return "Tytuł";
            case 'author':
                return "Autor";
            case 'publishDate':
                return "Data publikacji";
            case 'mediaType':
                return "Typ";
            case 'barcode':
                return "Kod kreskowy";
            case 'id':
                return "id";
            case 'status':
                return "Status";
            case 'ageLimitation':
                return "Ograniczenie wiekowe";

        }
        const field = this.state.fields
            .find(field => field.name == name);
        if (field !== undefined && field !== null) {
            return field.label;
        }
    };
}
