import CatalogPage from "./CatalogPage";
import DetailsPanel from "./DetailsPanel";
import EditPanel from "./EditPanel";

export default CatalogPage;
export {DetailsPanel, EditPanel}
