insert into user (name,
                  first_name,
                  last_name,
                  birth_date,
                  address,
                  gender,
                  role,
                  salt,
                  password)
values ('admin', 'Karol', 'Admiński', '2011-11-11', 'some really long address string', 'male',
        'ADMIN', X'909aacb1b22a61168abdd02fbe4147e114f5feadb57fadef8b006ea9a42a716f',
        X'8e5c7b73f626944bf1c0802c7488b8fb7555fe8bfd1db168b13b427887f120b0');
insert into user (name,
                  first_name,
                  last_name,
                  birth_date,
                  address,
                  gender,
                  role,
                  salt,
                  password)
values ('lib', 'Jan', 'Bibliotekarski', '2011-11-11', 'some really long address string', 'male',
        'LIBRARIAN', X'909aacb1b22a61168abdd02fbe4147e114f5feadb57fadef8b006ea9a42a716f',
        X'8e5c7b73f626944bf1c0802c7488b8fb7555fe8bfd1db168b13b427887f120b0');
insert into user (name,
                  first_name,
                  last_name,
                  birth_date,
                  address,
                  gender,
                  role,
                  salt,
                  password)
values ('user', 'Paweł', 'Wypożyczalski', '2011-11-11', 'some really long address string', 'male',
        'LIBRARIAN', X'909aacb1b22a61168abdd02fbe4147e114f5feadb57fadef8b006ea9a42a716f',
        X'8e5c7b73f626944bf1c0802c7488b8fb7555fe8bfd1db168b13b427887f120b0')
