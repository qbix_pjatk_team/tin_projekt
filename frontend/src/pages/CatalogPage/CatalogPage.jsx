import React, {Component} from 'react';
import CatalogHeader from "./CatalogHeader";
import CatalogPanel from "./CatalogPanel/CatalogPanel";
import './style.css';

export default class CatalogPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedIds: []
        };

    }

    render() {
        return (
            <div className="catalog-page">
                <CatalogHeader changeComponentTrigger={this.props.changeComponentTrigger}
                               user={this.props.user}
                               selectedIds={this.state.selectedIds}/>
                <CatalogPanel selectedIds={this.state.selectedIds} changeSelection={this.changeSelection}
                              user={this.props.user}
                              changeComponentTrigger={this.props.changeComponentTrigger}/>
            </div>
        );
    }


    changeSelection = (id) => {
        const ids = [...this.state.selectedIds];
        if (ids.includes(id)) {
            ids.splice(ids.indexOf(id), 1);
        } else {
            ids.push(id);
        }
        this.setState({selectedIds: ids});
    };
}
