import React, {Component} from "react";
import "./header.css";
import {componentNames} from "../MainApp";

export default class Header extends Component {
    render() {
        const {fireComponentChange} = this.props;
        return (
            <div className="header">
                <span
                    className="title"
                    onClick={() => fireComponentChange(componentNames.WELCOME_PAGE)}>
                    System mediateczny
                </span>
            </div>);
    }
}
