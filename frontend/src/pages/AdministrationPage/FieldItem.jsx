import React, {Component} from 'react';
import * as fieldsRepository from "../../repositories/fieldsRepository";
import {componentNames} from "../../MainApp";

export default class FieldItem extends Component {
    render() {
        const {field} = this.props;
        return (<div className="field-item">
            <input type="text" name="name" value={field.name} onChange={this.props.changeHandler}
                   placeholder="{nazwa pola}"/>
            <input type="text" name="label" value={field.label} onChange={this.props.changeHandler}
                   placeholder="{etykieta pola}"/>
            <input type="checkbox" name="required" onChange={this.props.changeHandler} checked={field.required}/>
            <select name="dataType" onChange={this.props.changeHandler}>
                {this.getTypesOptions()}
            </select>
            <input type="text" name="availableValues" onChange={this.props.changeHandler} value={field.availableValues}
                   placeholder="{dostępne wartości}"/>
            <button onClick={() => fieldsRepository.deleteField(field.id)
                .then(this.refreshPage)
                .catch(error => console.log(error.toString()))
            }>Usuń
            </button>
        </div>);
    }

    getTypesOptions = () => {
        return Object.values(fieldsRepository.dataTypes).map(
            type => <option key={type.name} selected={this.props.field.dataType === type.name}
                            value={type.name}>{type.label}</option>
        );
    };

    refreshPage = () => {
        this.props.changeComponentTrigger(componentNames.ADMINISTRATION_PAGE);
    };
}
