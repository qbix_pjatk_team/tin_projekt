import httpClient from "../utils/HttpClient";
import {baseUrl} from "../utils/settings";

const userUrl = `${baseUrl}/user`;

export const createUser = (user) => {
    return httpClient.post(userUrl, user);
};

export const login = (username, password) => {
    return httpClient.post(`${userUrl}/login`, {username: username, password: password}, true);
};

export const getUser = (id) => {
    return httpClient.get(`${userUrl}/${id}`);
};

export const getUsers = () => {
    return httpClient.get(userUrl);
};
