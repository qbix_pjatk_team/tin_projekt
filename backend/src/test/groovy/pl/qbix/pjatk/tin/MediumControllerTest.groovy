package pl.qbix.pjatk.tin


import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import pl.qbix.pjatk.tin.model.MediaStatus
import pl.qbix.pjatk.tin.model.Medium
import pl.qbix.pjatk.tin.repositories.MediumRepository

import java.time.LocalDate

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class MediumControllerTest extends IntegrationTest {
    @Autowired
    MediumRepository mediumRepository
    
    def "should return all data from contract"() {
        when:
            def result = mockMvc.perform(get("/medium"))
        
        then:
            result.andExpect(status().isOk())
                    .andExpect(jsonPath('$[0].id').isNumber())
                    .andExpect(jsonPath('$[0].mediaType').isString())
                    .andExpect(jsonPath('$[0].title').isString())
                    .andExpect(jsonPath('$[0].author').isString())
                    .andExpect(jsonPath('$[0].status').isString())
                    .andExpect(jsonPath('$[0].publishDate').isString())
    }
    
    def 'should properly create element'() {
        given:
            Map<String, String> data = [
                    mediaType  : "book",
                    author     : "Some author",
                    title      : "Some title",
                    status     : "dostępny",
                    publishDate: "2010-01-01"
            ]
    
        when:
            def result = mockMvc.perform(post("/medium")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data))
            )
    
        then:
            result
                    .andExpect(status().isCreated())
            def id = getId(result)
            mediumRepository.findById(id).isDefined()
    }
    
    def "should get medium by id"() {
        given:
            Long id = mediumRepository.save(someMedium()).getId()
        
        when:
            def result = mockMvc.perform(get("/medium/${id}"))
        
        then:
            result
                    .andExpect(status().isOk())
                    .andExpect(jsonPath('$.id').isNumber())
                    .andExpect(jsonPath('$.mediaType').isString())
                    .andExpect(jsonPath('$.title').isString())
                    .andExpect(jsonPath('$.author').isString())
                    .andExpect(jsonPath('$.status').isString())
                    .andExpect(jsonPath('$.publishDate').isString())
    
    }
    
    def "should also contain fields specific for given type"() {
        given:
            Map<String, String> data = [
                    mediaType  : "book",
                    author     : "Some author",
                    title      : "Some title",
                    status     : "dostępny",
                    publishDate: "2010-01-01",
                    genre      : "Kryminał",
                    pages      : 321
            ]
        when:
            def result = mockMvc.perform(post("/medium")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data)))
        
        then:
            result
                    .andExpect(status().isCreated())
            def id = getId(result)
            def medium = mediumRepository.findById(id).get()
            fieldValue(medium, "pages") == "321"
            fieldValue(medium, "genre") == "Kryminał"
    }
    
    def "should not allow field value outside of defined values for given type"() {
        given:
            Map<String, ?> data = [
                    mediaType  : "book",
                    author     : "Some author",
                    title      : "Some title",
                    status     : "dostępny",
                    publishDate: "2010-01-01",
                    genre      : "NonExistingGenre",
                    pages      : 321
            ]
        when:
            def result = mockMvc.perform(post("/medium")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data)))
        
        then:
            result
                    .andExpect(status().isBadRequest())
    }
    
    def "should allow to modify entity"() {
        given:
            Map<String, ?> data = [
                    mediaType  : "book",
                    author     : "Some other author",
                    title      : "Some other title",
                    status     : "wypożyczony",
                    publishDate: "2010-01-01",
                    genre      : "Kryminał",
                    pages      : 321
            ]
            def result = mockMvc.perform(post("/medium")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data)))
            def id = getId(result)
    
        when:
            data.genre = "Fantasy"
            data.pages = 123
            data.title = "title"
            result = mockMvc.perform(put("/medium/${id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data)))
    
        then:
            result.andExpect(status().isNoContent())
            def medium = mediumRepository.findById(id).get()
            medium.getTitle() == "title"
            fieldValue(medium, "genre") == "Fantasy"
            fieldValue(medium, "pages") == "123"
    }
    
    def "should return not found when trying to modify non existing entity"() {
        given:
            Map<String, ?> data = [
                    mediaType  : "book",
                    author     : "Some other author",
                    title      : "Some other title",
                    status     : "dostępny",
                    publishDate: "2010-01-01",
                    genre      : "Kryminał",
                    pages      : 321
            ]
            def result = mockMvc.perform(post("/medium")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data)))
            def id = getId(result) + 1
        
        when:
            result = mockMvc.perform(put("/medium/${id}")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(toJson(data)))
        
        then:
            result.andExpect(status().isNotFound())
    }
    
    def "should remove by id"() {
        given:
            Long id = mediumRepository.save(someMedium()).getId()
        
        when:
            def result = mockMvc.perform(delete("/medium/${id}"))
        
        then:
            result.andExpect(status().isNoContent())
    }
    
    private static Medium someMedium() {
        Medium.builder()
                .mediaType(pl.qbix.pjatk.tin.model.MediaType.BOOK)
                .title("Some title")
                .status(MediaStatus.AVAILABLE)
                .publishDate(LocalDate.parse("2019-01-01"))
                .author("SomeAuthor")
                .build()
    }
    
    String fieldValue(Medium medium, String fieldName) {
        return medium.getTypeSpecificFields()
                .stream()
                .filter({ field -> field.getField().getName() == fieldName })
                .findFirst()
                .get().getValue()
    }
}
