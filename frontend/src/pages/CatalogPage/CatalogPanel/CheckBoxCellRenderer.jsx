import React from 'react';

const CheckBoxCellRenderer = (selectedIds, onSelectionChange) => (row) => {
    return <div className="checkbox-cell">
        <input type="checkbox" checked={selectedIds.includes(row.original.id)}
               onChange={() => onSelectionChange(row.original.id)}
        />
    </div>;
};
export default CheckBoxCellRenderer
