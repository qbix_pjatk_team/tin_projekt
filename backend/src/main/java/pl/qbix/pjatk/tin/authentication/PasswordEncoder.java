package pl.qbix.pjatk.tin.authentication;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Random;

@Service
@AllArgsConstructor
public class PasswordEncoder {
    private final MessageDigestProvider messageDigestProvider;

    public PasswordEntity encode(String password) {
        var passBytes = password.getBytes();
        var saltBytes = generateSalt();
        return PasswordEntity.builder()
                .hashcode(encode(saltBytes, passBytes))
                .salt(saltBytes)
                .build();
    }

    byte[] encode(byte[] salt, byte[] password) {
        var merged = mergeBytes(salt, password);
        var messageDigest = messageDigestProvider.getMessageDigest();
        return messageDigest.digest(merged);
    }

    byte[] generateSalt() {
        var random = new Random();
        var saltBytes = new byte[32];
        random.nextBytes(saltBytes);
        return saltBytes;
    }

    byte[] mergeBytes(byte[] first, byte[] second) {
        byte[] newBytes = new byte[first.length + second.length];
        System.arraycopy(first, 0, newBytes, 0, first.length);
        System.arraycopy(second, 0, newBytes, first.length, second.length);
        return newBytes;
    }

    public boolean matches(String password, PasswordEntity passwordEntity) {
        var newPass = encode(passwordEntity.getSalt(), password.getBytes());
        return Arrays.equals(newPass, passwordEntity.getHashcode());
    }
}
