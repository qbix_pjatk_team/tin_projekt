import React, {Component} from 'react';
import * as fieldRepository from '../../repositories/fieldsRepository';
import * as mediaTypeRepository from '../../repositories/mediaTypeRepository';
import * as mediaRepository from '../../repositories/mediaRepository';
import {componentNames} from "../../MainApp";
import SingleValueField from "./SingleValueField";
import MultiValueField from "./MultiValueField";
import * as validations from "../../utils/validations";

export default class EditPanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            typeSpecificFields: [],
            values: props.data,
            mediaTypes: {},
            validations: []
        };
    }

    componentDidMount() {
        this.updateTypeSpecificFields();
        mediaTypeRepository.getMediaTypes()
            .then(types => this.setState({mediaTypes: types}))
            .then(() => {
                if (this.props.data !== null && this.props.data !== undefined && this.props.data.id > 0) {
                    this.validateFields();
                }
            });
    }

    render() {
        const data = this.state.values;
        return (
            <div className="edit-panel">
                {data.id && <label>id:</label>}
                {data.id && <label>{data.id}</label>}
                {data.id && <label> </label>}
                <label>Typ:</label>
                <select value={data.mediaType}
                        name="mediaTypes"
                        onChange={this.handleMediaChange}
                        onBlur={this.validateMediaType}>
                    <option>Wybierz</option>
                    {this.optionsForMediaTypes(this.state.mediaTypes)}
                </select>
                <label className="validationError">{this.state.validations.mediaType || ""}</label>
                <label>Tytuł:</label>
                <input type="text"
                       name="title"
                       value={data.title}
                       onChange={this.handleChange}
                       onBlur={this.validateTitle}/>
                <label className="validationError">{this.state.validations.title || ""}</label>
                <label>Autor:</label>
                <input type="text"
                       name="author"
                       value={data.author}
                       onChange={this.handleChange}
                       onBlur={this.validateAuthor}/>
                <label className="validationError">{this.state.validations.author || ""}</label>
                <label>Data wydania:</label>
                <input type="text"
                       name="publishDate"
                       value={data.publishDate}
                       onChange={this.handleChange}
                       onBlur={this.validatePublishDate}
                />
                <label className="validationError">{this.state.validations.publishDate || ""}</label>
                <label>Ograniczenie wiekowe:</label>
                <input type="text"
                       name="ageLimitation"
                       value={data.ageLimitation}
                       onChange={this.handleChange}
                       onBlur={this.validateAgeLimitation}
                />
                <label className="validationError">{this.state.validations.ageLimitation || ""}</label>
                <label>Kod kreskowy:</label>
                <input type="text"
                       name="barcode"
                       value={data.barcode}
                       onChange={this.handleChange}
                       onBlur={this.validateBarcode}
                />
                <label className="validationError">{this.state.validations.barcode || ""}</label>
                <label>Dostępne dla kleintow</label>
                <span>
                    <input type="radio" value={true} name="availableForBorrow" checked={data.availableForBorrow}
                           onChange={this.handleChange}/>Tak
                    <input type="radio" value={false} name="availableForBorrow" checked={!data.availableForBorrow}
                           onChange={this.handleChange}/>Nie
                </span>
                <label> </label>
                {this.typeSpecificFields()}
                <label className="errors">{this.errorMessage()}</label>
                <span className="buttons">
                    <button
                        onClick={() => {
                            mediaRepository.save(this.state.values)
                                .then(() => this.props.changeComponentTrigger(componentNames.CATALOG_PAGE));
                        }}
                        disabled={!this.isFormValid()}>Zapisz</button>
                    <button
                        onClick={() => this.props.changeComponentTrigger(componentNames.CATALOG_PAGE)}>Anuluj</button>
                </span>
            </div>
        );
    }

    handleChange = (event) => {
        const {target} = event;
        const field = target.name;
        let value = target.value;
        if (target.type === 'radio') {
            value = value === 'true';
        }
        this.setFieldValue(field, value);
    };

    handleMediaChange = (event) => {
        const {value} = event.target;
        this.setFieldValue("mediaType", value)
            .then(this.updateTypeSpecificFields)
            .then(this.validateMediaType);
    };

    optionsForMediaTypes = (mediaTypes) => {
        return Object.values(mediaTypes).map(
            type => <option key={type.name} value={type.name}>{type.label}</option>
        );
    };

    typeSpecificFields = () => {
        const data = this.state.values;
        const fields = [];
        this.state.typeSpecificFields
            .forEach(field => {
                    fields.push(<label key={"label" + field.id}>{field.label}</label>);
                    if (field.availableValues.length === 0) {
                        fields.push(<SingleValueField key={field.id}
                                                      field={field}
                                                      data={data[field.name]}
                                                      onChange={this.handleChange}
                                                      onBlur={this.validateTypeSpecificField}/>);
                    } else {
                        fields.push(<MultiValueField key={field.id}
                                                     field={field}
                                                     data={data[field.name]}
                                                     onChange={(event) => {
                                                         this.handleChange(event);
                                                         this.validateTypeSpecificField(event);
                                                     }}
                                                     onBlur={this.validateTypeSpecificField}/>);
                    }
                    fields.push(
                        <label className="validationError"
                               key={"error" + field.id}>{this.state.validations[field.name] || ""}</label>);
                }
            );
        return fields;
    };

    updateTypeSpecificFields = () => {
        const data = this.state.values;
        fieldRepository.getFieldsByMediaType(data.mediaType)
            .then((fields) => {
                this.state.typeSpecificFields.forEach(field => {
                    this.removeValidation(field.name);
                });
                return fields;
            })
            .then(fields => this.setState({typeSpecificFields: fields}));
    };

    validateFields = () => {
        this.validateMediaType();
        this.validateTitle();
        this.validateAuthor();
        this.validatePublishDate();
        this.validateAgeLimitation();
        this.validateBarcode();
        this.state.typeSpecificFields
            .map(
                field => {
                    return {
                        name: field.name,
                        value: this.state.values[field.name]
                    };
                }
            )
            .filter(fieldValue => fieldValue.value !== undefined)
            .forEach(fieldValue => {
                this.validateTypeSpecificFieldByDetails(fieldValue.name, fieldValue.value);
            })
        ;
    };

    validateMediaType = () => {
        let message = null;
        const value = this.state.values.mediaType;
        if (value === undefined) {
            message = "Typ zasobu musi być uzupełniony";
        }
        this.setValidationMessage("mediaType", message);
    };

    validateTitle = () => {
        this.setValidationMessage("title", validations.validateTitle(this.state.values.title));
    };

    validateAuthor = () => {
        this.setValidationMessage("author", validations.validateAuthor(this.state.values.author));
    };

    validatePublishDate = () => {
        this.setValidationMessage("publishDate", validations.validateDate(this.state.values.publishDate));
    };

    validateAgeLimitation = () => {
        this.setValidationMessage("ageLimitation", validations.validateAgeLimitation(this.state.values.ageLimitation));
    };

    validateBarcode = () => {
        this.setValidationMessage("barcode", validations.validateBarcode(this.state.values.barcode));
    };

    validateTypeSpecificField = (event) => {
        const {name, value} = event.target;
        this.validateTypeSpecificFieldByDetails(name, value);
    };

    validateTypeSpecificFieldByDetails(name, value) {
        const field = this.state.typeSpecificFields
            .find(field => field.name === name);
        let message = null;
        if (field.required && value === "") {
            message = "Pole nie może być puste";
        }
        if (value !== "") {
            message = validations.validateType(field.dataType, value);
        }
        this.setValidationMessage(field.name, message);
    }

    errorMessage = () => {
        return Object.values(this.state.validations)
            .filter(value => value != null)
            .length === 0 ? "" : "Formularz zawiera błędy";
    };

    isFormValid = () => {
        return this.errorMessage() === "" && Object.keys(this.state.validations).length === (6 + this.state.typeSpecificFields.length);
    };

    setValidationMessage = (field, value) => {
        return this.setAsyncState((prevState) => {
            return {
                validations: {
                    ...prevState.validations,
                    [field]: value
                }
            };
        });
    };

    setFieldValue = (field, value) => {
        return this.setAsyncState((prevState) => {
            return {
                values: {
                    ...prevState.values,
                    [field]: value
                }
            };
        });
    };

    removeValidation = (field) => {
        let val = {...this.state.validations};
        delete val[field];
        return this.setAsyncState({validations: val});
    };

    setAsyncState = (newState) => new Promise((resolve) => this.setState(newState, () => resolve()));
}
