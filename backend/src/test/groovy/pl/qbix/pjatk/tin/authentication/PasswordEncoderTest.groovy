package pl.qbix.pjatk.tin.authentication

import spock.lang.Specification
import spock.lang.Subject

class PasswordEncoderTest extends Specification {
    MessageDigestProvider messageDigestProvider = new MessageDigestProvider()
    
    @Subject
    PasswordEncoder encoder = new PasswordEncoder(messageDigestProvider)
    
    def "should return password entity based on password"() {
        given:
            def password = "haslotestowe"
        
        when:
            def result = encoder.encode(password)
        
        then:
            result != null
            result.hashcode != null
            result.salt != null
            result.hashcode != result.salt
            result.salt.length >= 32
            result.hashcode.length >= 32
    }
    
    def "should return different values for different paswords"() {
        given:
            def password1 = "haslotestowe"
            def password2 = "innehaslo"
        
        when:
            def result1 = encoder.encode(password1)
            def result2 = encoder.encode(password2)
        
        then:
            result1.hashcode != result2.hashcode
            result1.salt != result2.salt
    }
    
    def "should return match, if password is the same"() {
        given:
            def password = "testPassword"
            def passwordEntity = encoder.encode(password)
        
        when:
            def passwordMatch = encoder.matches(password, passwordEntity)
        
        then:
            passwordMatch
    }
    
    def "should not pass, if password is invalid"() {
        given:
            def password1 = "testPassword"
            def password2 = "otherPassword"
            def entity = encoder.encode(password1)
        
        when:
            def passwordMatch = encoder.matches(password2, entity)
        
        then:
            !passwordMatch
    }
}
