package pl.qbix.pjatk.tin.model;

public enum DataType {
    STRING, INTEGER
}
