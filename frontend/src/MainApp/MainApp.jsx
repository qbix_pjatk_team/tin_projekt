import React, {Component} from "react";
import Header from "../Header/";
import Sidebar from "../Sidebar/";

import "./main-app.css";
import {componentNames} from "./index";
import {AdministrationPage, CatalogPage, DetailsPanel, EditPanel, LoginPage, RegisterPage, WelcomePage} from "../pages";
import UserInfoPanel from "../UserInfoPanel";

export default class MainApp extends Component {


    state = {
        user: null,
        activeComponent: componentNames.WELCOME_PAGE,
        componentId: 1
    };

    render() {
        const {user} = this.state;
        return (
            <div className="root-component">
                <UserInfoPanel
                    user={user}
                    fireComponentChange={this.changeComponent}
                    logoutHandler={this.logout}/>
                <Header fireComponentChange={this.changeComponent}/>
                <Sidebar
                    user={user}
                    changeComponentHandler={this.changeComponent}
                    activeComponent={this.state.activeComponent}
                />
                {this.activeComponent()}
            </div>);
    }

    activeComponent = () => {
        const {activeComponent, componentProps} = this.state;
        switch (activeComponent) {
            case componentNames.WELCOME_PAGE:
                return <WelcomePage key={this.state.componentId}/>;
            case componentNames.LOGIN_PAGE:
                return <LoginPage onLogin={this.login} key={this.state.componentId}/>;
            case componentNames.REGISTER_PAGE:
                return <RegisterPage changeComponentTrigger={this.changeComponent}
                                     key={this.state.componentId}/>;
            case componentNames.CATALOG_PAGE:
                return <CatalogPage changeComponentTrigger={this.changeComponent} user={this.state.user}
                                    key={this.state.componentId}/>;
            case componentNames.ADMINISTRATION_PAGE:
                return <AdministrationPage key={this.state.componentId} changeComponentTrigger={this.changeComponent}/>;
            case componentNames.DETAILS_PAGE:
                return (
                    <DetailsPanel
                        key={componentProps.key}
                        data={componentProps.data}
                        changeComponentTrigger={this.changeComponent}/>);
            case componentNames.EDIT_PAGE:
                return (
                    <EditPanel
                        key={componentProps.key}
                        data={componentProps.data}
                        changeComponentTrigger={this.changeComponent}/>);
            default:
                return <div>View {activeComponent} not found</div>;
        }
    };

    changeComponent = (componentName, componentProps) => {
        this.setState({
            componentId: ++this.state.componentId,
            activeComponent: componentName,
            componentProps: componentProps
        });
    };

    login = (user) => {
        this.setState({
            componentId: ++this.state.componentId,
            user: user,
            activeComponent: componentNames.WELCOME_PAGE
        });
    };

    logout = () => {
        this.setState({
            user: null,
            componentId: ++this.state.componentId,
            activeComponent: componentNames.WELCOME_PAGE
        });
    };
}
