package pl.qbix.pjatk.tin.model;

public enum MediaType {
    BOOK, MOVIE
}
