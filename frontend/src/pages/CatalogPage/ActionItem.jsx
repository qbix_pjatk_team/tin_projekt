import React, {Component} from 'react';

export default class ActionItem extends Component {
    render() {
        const {label, clickHandler} = this.props;
        return (
            <span className="action-item">
                <a href="#" onClick={clickHandler}>
                    {label}
                </a>
            </span>
        )
    }
}
