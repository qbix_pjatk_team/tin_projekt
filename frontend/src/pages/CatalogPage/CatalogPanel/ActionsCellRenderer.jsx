import React from 'react';
import ActionItem from "../ActionItem";
import {componentNames} from "../../../MainApp";

const ActionsCellRenderer = (changeComponentTrigger, userRole) =>
    (row) => {
        return (
            <div>
                {["ADMIN", "LIBRARIAN"].includes(userRole) &&
                <ActionItem label="Edycja" clickHandler={() => changeComponentTrigger(
                    componentNames.EDIT_PAGE,
                    {
                        key: row.original.id,
                        data: row.original
                    }
                )
                }/>}
                <ActionItem label="Szczegóły" clickHandler={() => changeComponentTrigger(
                    componentNames.DETAILS_PAGE,
                    {
                        key: row.original.id,
                        data: row.original
                    }
                )}/>
            </div>
        );
    };
export default ActionsCellRenderer;
