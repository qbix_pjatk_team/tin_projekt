import MainApp from "./MainApp";

export const componentNames = {
    WELCOME_PAGE: "WELCOME_PAGE",
    LOGIN_PAGE: "LOGIN_PAGE",
    REGISTER_PAGE: "REGISTER_PAGE",
    CATALOG_PAGE: "CATALOG_PAGE",
    ADMINISTRATION_PAGE: "ADMINISTRATION_PAGE",
    DETAILS_PAGE: "DETAILS_PAGE",
    EDIT_PAGE: "EDIT_PAGE"
};
export default MainApp
