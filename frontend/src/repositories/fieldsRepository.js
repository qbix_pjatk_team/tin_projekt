import httpClient from "../utils/HttpClient";
import {baseUrl} from "../utils/settings";

const fieldUrl = `${baseUrl}/field`;

export const getFieldsByMediaType = (mediaType) => {
    return httpClient.get(`${fieldUrl}?mediaType=${mediaType}`);
};

export const updateFields = (fields) => {
    const fieldsToSend = fields
        .map(field => {
            if (field.id < 0) {
                return {...field, id: null};
            } else {
                return field;
            }
        });
    return httpClient.put(`${fieldUrl}`, fieldsToSend);
};

export const deleteField = (fieldId) => {
    return httpClient.delete(`${fieldUrl}/${fieldId}`);
};

export const dataTypes = {
    text: {
        name: "STRING",
        label: "Tekst"
    },
    integer: {
        name: "INTEGER",
        label: "Liczba"
    }
};
