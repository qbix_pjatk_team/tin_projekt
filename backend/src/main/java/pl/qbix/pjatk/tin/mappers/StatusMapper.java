package pl.qbix.pjatk.tin.mappers;

import io.vavr.collection.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.qbix.pjatk.tin.model.MediaStatus;

@Service
public class StatusMapper {
    public String toDto(MediaStatus mediaStatus) {
        return Mappings.valueOf(mediaStatus.name()).tag;
    }

    public MediaStatus fromString(String mediaType) {
        return List.of(StatusMapper.Mappings.values())
                .find(type -> type.tag.equals(mediaType))
                .map(type -> MediaStatus.valueOf(type.name()))
                .getOrElse(MediaStatus.AVAILABLE);
    }

    @AllArgsConstructor
    private enum Mappings {
        AVAILABLE("dostępny"),
        BORROWED("wypożyczony");
        private String tag;
    }
}
