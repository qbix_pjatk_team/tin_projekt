package pl.qbix.pjatk.tin.repositories;

import io.vavr.control.Option;
import pl.qbix.pjatk.tin.model.user.User;

public interface UserRepository extends MyBaseJPARepository<User, Long> {
    Option<User> findByName(String name);
}
