package pl.qbix.pjatk.tin.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@AllArgsConstructor
@Builder
@Data
public class MediaTypeDTO {
    String name;
    String label;
}
