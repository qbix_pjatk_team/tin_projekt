package pl.qbix.pjatk.tin.repositories;

import pl.qbix.pjatk.tin.model.Medium;

public interface MediumRepository extends MyBaseJPARepository<Medium, Long> {
}
