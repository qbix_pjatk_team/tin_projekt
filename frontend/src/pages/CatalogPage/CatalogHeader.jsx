import React, {Component} from 'react';
import ActionItem from "./ActionItem";
import Modal from 'react-modal';
import {componentNames} from "../../MainApp";
import * as mediaRepository from "../../repositories/mediaRepository";
import * as borrowRepository from "../../repositories/borrowRepository";
import * as userRepository from "../../repositories/userRepository";

export default class CatalogHeader extends Component {

    state = {
        errors: "",
        dialogOpen: false,
        users: [{
            id: 1,
            firstName: "Jan",
            lastName: "Kowalski"
        }]
    };

    render() {
        return (
            <div className="catalog-header">
                <div className="errors">{this.state.errors}</div>
                {this.props.user && ["ADMIN", "LIBRARIAN"].includes(this.props.user.userRole) &&
                <span>
                <ActionItem label="Dodaj zasób" clickHandler={() =>
                    this.props.changeComponentTrigger(
                        componentNames.EDIT_PAGE,
                        {key: -1, data: {}}
                    )}/>
                <ActionItem label="Usuń zaznaczenie"
                            clickHandler={
                                () => mediaRepository.deleteIds(this.props.selectedIds)
                                    .then(this.refreshPage)
                                    .catch(this.displayError)
                            }
                />
                <ActionItem label="Wypożycz użytkownikowi"
                            clickHandler={this.openDialog}
                />
                <ActionItem label="Zarejestruj zwrot"
                            clickHandler={
                                () => {
                                    borrowRepository.returnBorrows(this.props.selectedIds)
                                        .then(this.refreshPage)
                                        .catch(this.displayError);
                                }
                            }/>
                    </span>
                }
                {this.state.dialogOpen && <Modal
                    style={{
                        content: {
                            top: '50%',
                            left: '50%',
                            right: 'auto',
                            bottom: 'auto',
                            marginRight: '-50%',
                            backgroundColor: "darkseagreen",
                            transform: 'translate(-50%, -50%)'
                        }
                    }}
                    contentLabel="Example Modal"
                    isOpen={this.state.dialogOpen}>
                    <div className="user-modal">
                        <h1>Wybierz użytkownika</h1>
                        <ul>
                            {this.state.users.map(user => <li key={user.id} onClick={() => {
                                const borrows = this.props.selectedIds
                                    .map(id => {
                                        return {
                                            userId: user.id,
                                            mediumId: id
                                        };
                                    });
                                borrowRepository.borrow(borrows)
                                    .then(this.refreshPage)
                                    .catch(this.displayError)
                                    .finally(this.closeDialog);
                            }}>{user.firstName} {user.lastName}</li>)}
                        </ul>
                        <button onClick={this.closeDialog}>Anuluj</button>
                    </div>
                </Modal>}
            </div>
        );
    }

    closeDialog = () => {
        this.setState({dialogOpen: false});
    };

    openDialog = () => {
        userRepository.getUsers()
            .then(users => this.setState({
                users: users,
                dialogOpen: true
            }))
            .catch(this.displayError);
    };

    displayError = (error) => {
        this.setState({errors: error.toString()});
    };

    refreshPage = () => {
        this.props.changeComponentTrigger(componentNames.CATALOG_PAGE);
    };
}
