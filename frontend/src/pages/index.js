import CatalogPage, {DetailsPanel, EditPanel} from "./CatalogPage";
import LoginPage from "./LoginPage";
import RegisterPage from "./RegisterPage";
import WelcomePage from "./WelcomePage";
import AdministrationPage from "./AdministrationPage";

export {CatalogPage, LoginPage, RegisterPage, WelcomePage, AdministrationPage, DetailsPanel, EditPanel}
