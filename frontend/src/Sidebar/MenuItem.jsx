import React, {Component} from 'react';

export default class MenuItem extends Component {
    render() {
        const {label, clickHandler} = this.props;
        return (
            <div className={"menu-item " + (this.props.isActive ? "active" : "")}>
                <a href="#" onClick={clickHandler}>
                    {label}
                </a>
            </div>
        )
    }
}
