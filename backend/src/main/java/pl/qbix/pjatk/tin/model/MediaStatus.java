package pl.qbix.pjatk.tin.model;

public enum MediaStatus {
    AVAILABLE, BORROWED
}
