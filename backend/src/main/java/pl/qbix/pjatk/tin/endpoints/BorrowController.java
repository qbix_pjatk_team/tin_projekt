package pl.qbix.pjatk.tin.endpoints;

import io.vavr.collection.List;
import io.vavr.collection.Traversable;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.qbix.pjatk.tin.dto.borrow.BorrowDTO;
import pl.qbix.pjatk.tin.model.Borrow;
import pl.qbix.pjatk.tin.model.MediaStatus;
import pl.qbix.pjatk.tin.repositories.BorrowRepository;
import pl.qbix.pjatk.tin.repositories.MediumRepository;
import pl.qbix.pjatk.tin.repositories.UserRepository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/borrow")
@AllArgsConstructor
public class BorrowController {
    private final UserRepository userRepository;
    private final MediumRepository mediumRepository;
    private final BorrowRepository borrowRepository;

    @PostMapping
    @Transactional
    public void borrow(@RequestBody List<BorrowDTO> borrowsDTO) {
        borrowsDTO
                .forEach(this::borrow);
    }

    private void borrow(BorrowDTO borrowDTO) {
        if (isCurrentlyBorrowed(borrowDTO)) {
            throw new IllegalArgumentException("Książka już w wypożyczeniu");
        }
        var medium =
                mediumRepository.findById(borrowDTO.getMediumId()).getOrElseThrow(() -> new IllegalArgumentException("Taki zasób nie istnieje"));
        var borrow = Borrow.builder()
                .startDate(LocalDateTime.now())
                .medium(medium)
                .user(userRepository.findById(borrowDTO.getUserId()).getOrElseThrow(() -> new IllegalArgumentException("Taki użytkownik nie istnieje")))
                .build();
        borrowRepository.save(borrow);
        medium.setStatus(MediaStatus.BORROWED);
        mediumRepository.save(medium);
    }

    private boolean isCurrentlyBorrowed(BorrowDTO borrowDTO) {
        var allBorrows = borrowRepository
                .findByMediumId(borrowDTO.getMediumId());
        return allBorrows
                .filter(borrow -> borrow.getEndDate() == null)
                .size() > 0;
    }

    @PutMapping
    @Transactional
    public void returnBorrows(@RequestBody List<Long> mediumIds) {
        borrowRepository
                .findAllByMediumIdIn(mediumIds.toJavaList())
                .filter(borrow -> borrow.getEndDate() == null)
                .peek(borrow -> borrow.setEndDate(LocalDateTime.now()))
                .forEach(borrow -> {
                    borrowRepository.save(borrow);
                    borrow.getMedium().setStatus(MediaStatus.AVAILABLE);
                    mediumRepository.save(borrow.getMedium());
                });
    }

    @GetMapping
    public Traversable<Borrow> getBorrows() {
        return borrowRepository.findAll();
    }
}
