import React, {Component} from 'react';

export default class MultiValueField extends Component {
    render() {
        const {field, data} = this.props;
        return (
            <select
                value={data}
                name={field.name}
                onChange={this.props.onChange}
                onBlur={this.props.onBlur}>
                <option value="">Wybierz</option>
                {this.getOptionsFromAvailableValues(field.availableValues)}
            </select>
        );
    }

    getOptionsFromAvailableValues = (availableValues) => {
        return availableValues
            .map(value => <option value={value} key={value}>{value}</option>);
    };
}
