import httpClient from "../utils/HttpClient";
import {baseUrl} from "../utils/settings";

const mediaTypeUrl = `${baseUrl}/media-type`;

export const getMediaTypes = () => {
    return httpClient.get(mediaTypeUrl);
};

export const getMediaTypeByName = (name) => {
    return getMediaTypes()
        .then(data => Object.values(data)
            .filter(data => data.name === name));
};
