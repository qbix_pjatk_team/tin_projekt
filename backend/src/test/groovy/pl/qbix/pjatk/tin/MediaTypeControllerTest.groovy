package pl.qbix.pjatk.tin


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class MediaTypeControllerTest extends IntegrationTest {
    def "should return all data from contract"() {
        when:
            def resultActions = mockMvc.perform(
                    get("/media-type"))
        
        then:
            resultActions
                    .andExpect(status().isOk())
                    .andExpect(jsonPath('$.book.name').value("book"))
                    .andExpect(jsonPath('$.book.label').value("Książka"))
    }
}
