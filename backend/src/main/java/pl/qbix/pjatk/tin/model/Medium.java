package pl.qbix.pjatk.tin.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;
import static lombok.AccessLevel.PRIVATE;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Medium {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Setter(PRIVATE)
    private Long id;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MediaType mediaType;

    @NotNull
    @Column(nullable = false)
    private String title;

    @NotNull
    @Column(nullable = false)
    private String author;

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private MediaStatus status;

    @NotNull
    @Column(nullable = false)
    private LocalDate publishDate;

    private Integer ageLimitation;

    @NotNull
    @Column(nullable = false)
    private String barcode;

    @OneToMany(mappedBy = "medium", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FieldValue> typeSpecificFields;

    public void setTypeSpecificFields(List<FieldValue> fields) {
        if (typeSpecificFields == null) {
            typeSpecificFields = new ArrayList<>();
        }
        typeSpecificFields.clear();
        typeSpecificFields.addAll(fields);
    }
}
