create table borrow
(
    ID         IDENTITY  NOT NULL PRIMARY KEY,
    start_date timestamp not null,
    end_date   timestamp,
    user_id    bigint    not null,
    medium_id  bigint    not null,
    foreign key (user_id) references user (id),
    foreign key (medium_id) references medium (id)
)
