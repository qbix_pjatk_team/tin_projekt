package pl.qbix.pjatk.tin.repositories;

import io.vavr.collection.Traversable;
import org.springframework.data.repository.query.Param;
import pl.qbix.pjatk.tin.model.Field;
import pl.qbix.pjatk.tin.model.MediaType;

public interface FieldRepository extends MyBaseJPARepository<Field, Long> {
    Traversable<Field> findAllByMediaType(@Param("mediaType") MediaType mediaType);
}
