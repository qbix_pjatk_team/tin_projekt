CREATE TABLE user
(
    ID         IDENTITY     NOT NULL PRIMARY KEY,
    name       varchar(20)  not null unique,
    first_name varchar(20)  not null,
    last_name  varchar(20)  not null,
    birth_date date         not null,
    address    varchar(100) not null,
    gender     varchar(10)  not null,
    role       varchar(10)  not null,
    salt       binary(32)   not null,
    password   binary(32)   not null
)
