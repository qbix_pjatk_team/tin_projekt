import React, {Component} from 'react';
import './style.css';
import * as validations from '../../utils/validations';
import * as userRepository from '../../repositories/userRepository';
import {componentNames} from "../../MainApp";

export default class RegisterPage extends Component {
    state = {
        validations: {},
        values: {
            login: "",
            firstName: "",
            lastName: "",
            birthDate: "",
            address: "",
            password: "",
            passwordRepeat: ""
        },
        externalError: ""
    };

    render(props) {
        const {values} = this.state;
        return <div className='register-page'>
            <div className="register-form">
                <label>Login</label>
                <input type="text"
                       name="login"
                       onBlur={this.validateLogin}
                       value={values.login}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.login || ""}</label>
                <label>Imię</label>
                <input type="text"
                       name="firstName"
                       onBlur={this.validateFirstName}
                       value={values.firstName}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.firstName || ""}</label>
                <label>Nazwisko</label>
                <input type="text"
                       name="lastName"
                       onBlur={this.validateLastName}
                       value={values.lastName}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.lastName || ""}</label>
                <label>Data urodzenia</label>
                <input type="text" name="birthDate" onBlur={this.validateBirthDate}
                       value={values.birthDate}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.birthDate || ""}</label>
                <label>Adres</label>
                <input type="text" name="address" onBlur={this.validateAddress}
                       value={values.address}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.address || ""}</label>
                <label>Hasło</label>
                <input type="password" name="password" onBlur={this.validatePassword}
                       value={values.password}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.password || ""}</label>
                <label>Powtórz hasło</label>
                <input type="password" name="passwordRepeat" onBlur={this.validatePasswordRepeat}
                       value={values.passwordRepeat}
                       onChange={this.handleChange}/>
                <label className="validationError">{this.state.validations.passwordRepeat || ""}</label>
                <label>Płeć</label>
                <div className="gender">
                    <span>
                    <input type="radio" name="gender" value="male" onChange={this.genderSelected}/>Mężczyzna
                    </span>
                    <span>
                    <input type="radio" name="gender" value="female" onChange={this.genderSelected}/>Kobieta
                    </span>
                </div>
                <label className="validationError">{this.state.validations.gender || ""}</label>
                <div className="errors">{`${this.errorMessage()}${this.state.externalError}`}</div>
                <div className="buttons">
                    <button disabled={!this.formValid()} onClick={this.save}>Zarejestruj
                    </button>
                </div>
            </div>
        </div>;
    }

    save = () => {
        userRepository.createUser(this.state.values)
            .then(() => this.props.changeComponentTrigger(componentNames.WELCOME_PAGE))
            .catch(error => {
                console.log(error);
                this.setState({externalError: error});
            });
    };

    validateLogin = (event) => {
        const {value} = event.target;
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    login: validations.validateLogin(value)
                }
            };
        });
    };

    validateFirstName = (event) => {
        const {value} = event.target;
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    firstName: validations.validateFirstName(value)
                }
            };
        });
    };

    validateLastName = (event) => {
        const {value} = event.target;
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    lastName: validations.validateLastName(value)
                }
            };
        });
    };

    validateBirthDate = (event) => {
        const {value} = event.target;
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    birthDate: validations.validateDate(value)
                }
            };
        });
    };

    validateAddress = (event) => {
        const {value} = event.target;
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    address: validations.validateAddress(value)
                }
            };
        });
    };

    validatePassword = (event) => {
        const {value} = event.target;
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    password: validations.validatePassword(value)
                }
            };
        });
    };

    validatePasswordRepeat = () => {
        let message = null;
        if (this.state.values.passwordRepeat !== this.state.values.password) {
            message = "Hasła się nie zgadzają";
        }
        this.setState(() => {
            return {
                validations: {
                    ...this.state.validations,
                    passwordRepeat: message
                }
            };
        });
    };

    genderSelected = (event) => {
        var {value} = event.target;
        this.setState(prevState => {
            return {
                validations: {
                    ...prevState.validations,
                    gender: null
                },
                values: {
                    ...prevState.values,
                    gender: value
                }
            };
        });
    };

    errorMessage = () => {
        return Object.values(this.state.validations)
            .filter(value => value != null)
            .length === 0 ? "" : "Formularz zawiera błędy";
    };

    formValid = () => {
        return Object.keys(this.state.validations).length === 8 && this.errorMessage() === "";
    };

    handleChange = (event) => {
        const {name, value} = event.target;
        const values = {...this.state.values};
        values[name] = value;
        this.setState({values: values});
    };
}

