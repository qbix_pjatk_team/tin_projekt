import React, {Component} from 'react';
import './style.css';
import * as userRepository from "../../repositories/userRepository";

export default class LoginPage extends Component {
    state = {
        login: "",
        password: "",
        error: ""
    };

    render() {
        return (
            <div className="login-page">
                <div className="login-form">
                    <label>login</label>
                    <input type="text" name="login" value={this.state.login} onChange={this.changeHandler}/>
                    <label>hasło</label>
                    <input type="password" name="password" value={this.state.password} onChange={this.changeHandler}/>
                    <div className="errors">{this.state.error}</div>
                    <div className="buttons">
                        <button onClick={this.login}>Zaloguj</button>
                    </div>
                </div>
            </div>
        );
    }

    changeHandler = (event) => {
        const {name, value} = event.target;
        this.setState({[name]: value});
    };

    login = () => {
        userRepository.login(this.state.login, this.state.password)
            .then((user) => {
                console.log(user);
                this.props.onLogin(user);
            })
            .catch(error => {
                this.setState({error: error.toString()});
            });
    };
}
