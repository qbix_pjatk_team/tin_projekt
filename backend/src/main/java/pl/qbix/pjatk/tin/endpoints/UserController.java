package pl.qbix.pjatk.tin.endpoints;

import io.vavr.collection.Traversable;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.qbix.pjatk.tin.authentication.PasswordEncoder;
import pl.qbix.pjatk.tin.authentication.PasswordEntity;
import pl.qbix.pjatk.tin.dto.user.AuthenticationRequest;
import pl.qbix.pjatk.tin.dto.user.SimplifiedUserDTO;
import pl.qbix.pjatk.tin.dto.user.UserDTO;
import pl.qbix.pjatk.tin.mappers.UserMapper;
import pl.qbix.pjatk.tin.repositories.UserRepository;

import static org.springframework.http.HttpStatus.CREATED;
import static pl.qbix.pjatk.tin.model.user.UserRole.USER;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
@Slf4j
public class UserController {
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;

    @PostMapping
    @ResponseStatus(CREATED)
    public void createUser(@RequestBody UserDTO userDTO) {
        var curentUser = userRepository.findByName(userDTO.getLogin());
        if (curentUser.isDefined()) {
            throw new IllegalArgumentException("Użytkownik o podanym loginie już istnieje");
        }
        var user = userMapper.mapFromDto(userDTO);
        var passwordEntity = passwordEncoder.encode(userDTO.getPassword());
        user.setPassword(passwordEntity.getHashcode());
        user.setSalt(passwordEntity.getSalt());
        user.setRole(USER);
        userRepository.save(user);
    }

    @PostMapping("/login")
    public SimplifiedUserDTO login(@RequestBody AuthenticationRequest request) {
        var user = userRepository.findByName(request.getUsername())
                .getOrElseThrow(() -> new IllegalArgumentException("Zła nazwa użytkownika lub hasło"));
        var passwordEntity = PasswordEntity.builder()
                .hashcode(user.getPassword())
                .salt(user.getSalt())
                .build();
        if (!passwordEncoder.matches(request.getPassword(), passwordEntity)) {
            throw new IllegalArgumentException("Zła nazwa użytkownika lub hasło");
        } else {
            return userMapper.toSimplifiedDTO(user);
        }
    }

    @GetMapping("/{userId}")
    public SimplifiedUserDTO getUser(@PathVariable Long userId) {
        return userRepository
                .findById(userId)
                .map(userMapper::toSimplifiedDTO)
                .getOrElseThrow(() -> new IllegalArgumentException("Brak użytkownika"));
    }

    @GetMapping
    public Traversable<SimplifiedUserDTO> getUsers() {
        return userRepository
                .findAll()
                .map(userMapper::toSimplifiedDTO);
    }
}
