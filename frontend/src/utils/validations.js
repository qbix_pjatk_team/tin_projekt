const validateLogin = (value) => {
    const textValidation = validateTextField(value, "Login", 5);
    if (textValidation) return textValidation;
    if (isNotAlphanumeric(value)) {
        return 'Login może się składać tylko z liter i cyfr';
    }
    return null;
};

const validateFirstName = (value) => {
    const textValidation = validateTextField(value, "Imię", 3);
    if (textValidation) return textValidation;
    if (isNotAlphabetic(value)) {
        return `Imię może zawierać wyłącznie litery`;
    }
    return null;
};

const validateLastName = (value) => {
    const textValidation = validateTextField(value, "Nazwisko", 3);
    if (textValidation) return textValidation;
    if (!value.match(/^\w+(-\w+){0,1}$/)) {
        return "Naziwsko musi się składać z samych liter (ewentualnie oddzielonych myślnikiem)";
    }
    return null;
};

const validateDate = (value) => {
    if (isEmpty(value)) {
        return "Data nie może być pusta";
    }
    if (!isProperDate(value)) {
        return "Data musi być w formacie yyyy-MM-dd";
    }
    return null;
};

const validateAddress = (value) => {
    return validateTextField(value, "Adres", 15);
};

const validatePassword = (value) => {
    const textValidation = validateTextField(value, "Hasło", 8);
    if (textValidation) return textValidation;
    if (!value.match(/\d+/) ||
        !value.match(/[A-Z]+/) ||
        !value.match(/[a-z]+/) ||
        value.match(/^\w+$/)) {
        return "Hasło musi się składać z conamjniej jednej cyfry, małe litery, dużej litery i znaku specjalnego";
    }
    return null;
};

const validateTitle = (value) => {
    return validateTextField(value, "Tytuł", 3);
};

const validateAuthor = (value) => {
    return validateTextField(value, "Autor", 3);
};

const validateAgeLimitation = (value) => {
    if (value) {
        if (!value.match(/^\d*$/)) return "Ograniczenie wiekowe musi być dodanią liczbą całkowitą";
    }
    return null;
};

const validateBarcode = (value) => {
    const textValidation = validateTextField(value, "Kod kreskowy", 10);
    if (textValidation) return textValidation;
    if (!value.match(/^\d+(-\d+)*$/)) {
        return "Kod kreskowy musi składać się wyłącznie z cyfr oddzielonych myślnikami";
    }
};

const validateTextField = (value, label, minimalLength) => {
    if (isEmpty(value)) {
        return `Pole nie może być puste`;
    }
    if (value.length < minimalLength) {
        return `Pole nie może być krótsze, niż ${minimalLength}`;
    }
    return null;
};

const validateType = (type, value) => {
    if (type === 'INTEGER') {
        return validateInteger(value);
    }
    return null;
};

const validateInteger = (value) => {
    if (!value.match(/^\d*$/)) {
        return "Pole musi być liczbą całkowitą";
    }
    return null;
};

const isNotAlphabetic = (value) => !isAlphabetic(value);

const isAlphabetic = (value) => value.match(/^\w+$/);

const isNotAlphanumeric = (value) => !isAlphanumeric(value);

const isAlphanumeric = (value) => value.match(/^[a-zA-Z0-9]+$/);

const isEmpty = (value) => {
    return value === undefined || value === null || value === '';
};

const isProperDate = (value) => value.match(/^\d{4}-\d{2}-\d{2}$/);

export {
    validateLogin,
    validateFirstName,
    validateLastName,
    validateDate,
    validateTitle,
    validateAddress,
    validatePassword,
    validateAuthor,
    validateAgeLimitation,
    validateBarcode,
    validateType
};
