package pl.qbix.pjatk.tin.authentication;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;

@Builder
@Value
public class PasswordEntity {
    @NonNull
    byte[] hashcode;
    @NonNull
    byte[] salt;
}
