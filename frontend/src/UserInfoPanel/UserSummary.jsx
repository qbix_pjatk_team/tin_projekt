import React, {Component} from 'react';

export default class UserSummary extends Component {
    render() {
        const user = this.props.user;
        return (
            <div className="user-summary">
                Użytkownik: {user.firstName} {user.lastName} ({user.username})<br/>
                Rola: {this.props.user.userRole}<br/>
            </div>
        );
    }
}
